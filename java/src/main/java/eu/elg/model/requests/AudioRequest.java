/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.requests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.elg.model.Request;

/**
 * Request representing a piece of audio - the actual audio data will be sent as a separate part of a multipart
 * request.
 */
public class AudioRequest extends Request<AudioRequest> {

  /**
   * Enumeration of available audio formats.
   */
  public static enum Format {
    LINEAR16, MP3
  }

  private Format format;
  private Integer sampleRate;

  /**
   * The audio format of the request data - optional if the data is self-describing.
   */
  @JsonProperty("format")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public Format getFormat() {
    return format;
  }

  /**
   * Set the data format, if the data is not in a format with a self-describing header.
   *
   * @param format the format of the audio data.
   */
  @JsonProperty("format")
  public void setFormat(Format format) {
    this.format = format;
  }

  /**
   * Fluent setter for the {@link #getFormat format property}
   *
   * @param format the format of the audio data
   * @return this object, for chaining
   */
  public AudioRequest withFormat(Format format) {
    setFormat(format);
    return this;
  }

  /**
   * Sample rate of the audio in Hz - optional if this is encapsulated in the file header.
   */
  @JsonProperty("sampleRate")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public Integer getSampleRate() {
    return sampleRate;
  }

  /**
   * Set the sample rate of the audio in Hz, if the data is not in a format with a self-describing header.
   *
   * @param sampleRate the audio sample rate in Hz
   */
  @JsonProperty("sampleRate")
  public void setSampleRate(Integer sampleRate) {
    this.sampleRate = sampleRate;
  }

  /**
   * Fluent setter for the {@link #getSampleRate sample rate property}
   *
   * @param sampleRate the audio sample rate in Hz
   * @return this object, for chaining
   */
  public AudioRequest withSampleRate(Integer sampleRate) {
    setSampleRate(sampleRate);
    return this;
  }

  public String type() {
    return "audio";
  }
}
