/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import eu.elg.model.Markup;
import eu.elg.model.Response;

import java.net.URI;

/**
 * Response representing audio data with optional standoff annotations (e.g. text-to-speech results).
 */
public class AudioResponse extends Response<AudioResponse> {
  private byte[] content;
  private URI contentLocation;
  private Format format;
  private Markup markup;

  /**
   * Enumeration of available audio formats.
   */
  public static enum Format {
    LINEAR16, MP3
  }

  /**
   * Audio content to return
   */
  @JsonProperty("content")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public byte[] getContent() {
    return content;
  }

  /**
   * Set audio to return - will be encoded as a base64 string in the resulting JSON so should only be used for
   * relatively small requests.
   *
   * @param content the audio to return, in a supported format.
   */
  @JsonProperty("content")
  public void setContent(byte[] content) {
    this.content = content;
  }

  /**
   * Fluent setter for the {@link #getContent content property}
   *
   * @param content the audio to return, in a supported format.
   * @return this object, for chaining.
   */
  public AudioResponse withContent(byte[] content) {
    setContent(content);
    return this;
  }

  /**
   * The content, as a reference to a location in object storage.
   */
  @JsonProperty("contentLocation")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public URI getContentLocation() {
    return contentLocation;
  }

  /**
   * Set the content location in object storage
   *
   * @param contentLocation location of the content in storage
   */
  @JsonProperty("contentLocation")
  public void setContentLocation(URI contentLocation) {
    this.contentLocation = contentLocation;
  }

  /**
   * Fluent setter for the {@link #getContentLocation content location property}
   *
   * @param contentLocation location of the content in storage
   * @return this object, for chaining
   */
  public AudioResponse withContentLocation(URI contentLocation) {
    setContentLocation(contentLocation);
    return this;
  }

  /**
   * Convenience fluent setter for {@link #getContentLocation content location}, taking a URI string rather than a
   * {@link URI}
   *
   * @param contentLocation location of the content in storage
   * @return this object, for chaining
   * @throws IllegalArgumentException if the specified string is not a valid URI
   */
  public AudioResponse withContentLocation(String contentLocation) {
    return withContentLocation(URI.create(contentLocation));
  }

  /**
   * The audio format of the response data - optional if the data is self-describing.
   */
  @JsonProperty("format")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public Format getFormat() {
    return format;
  }

  /**
   * Set the data format, if the data is not in a format with a self-describing header.
   *
   * @param format the format of the audio data.
   */
  @JsonProperty("format")
  public void setFormat(Format format) {
    this.format = format;
  }

  /**
   * Fluent setter for the {@link #getFormat format property}
   *
   * @param format the format of the audio data
   * @return this object, for chaining
   */
  public AudioResponse withFormat(Format format) {
    setFormat(format);
    return this;
  }

  /**
   * The markup (annotations and features) on this text.
   * @return the current markup.
   */
  @JsonUnwrapped
  public Markup getMarkup() {
    return markup;
  }

  /**
   * Set the markup on this text.
   * @param markup the markup to use.
   */
  @JsonUnwrapped
  public void setMarkup(Markup markup) {
    this.markup = markup;
  }

  /**
   * Fluent setter for the {@link #getMarkup() markup} property.
   *
   * @param markup the markup to use
   * @return this object, for chaining
   */
  public AudioResponse withMarkup(Markup markup) {
    setMarkup(markup);
    return this;
  }}
