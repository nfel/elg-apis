ELG proposals for public-facing APIs
===================

## Preamble

- Maintainers: University of Sheffield: Ian Roberts & David R Jones
- Status: Draft

## Introduction

In addition to the "internal" APIs described in [T2.5 API Design](T2.5-api-design.md) for communication internally among components of the ELG platform, the project also needs to specify the kinds of APIs that will be made available to allow public use of the integrated tools.  There are two broad approaches we could take to this task:

  1. Define a uniform family of ELG-specific APIs that make sense for the various classes of tools, probably closely related to the existing internal message formats
  2. For each class of tools, pick one (or more) existing de jure or de facto standard third party APIs and aim to be wire-compatible with these, so clients that are already set up to use the existing API can transition easily to the ELG

In practice a combination of these two will probably be the most appropriate - where well defined and documented third-party APIs exist already and operate in a way that is compatible with the ELG processing model, they can be used, but where the ELG needs to support processing models that are not handled well by existing APIs then we must define our own.  We should also be concious not to tie the ELG too tightly to any particular third-party API specification over which we have no control.

In an ideal world, the web-based user interfaces of the ELG portal should as far as possible make calls to the same public API endpoints (though possibly with differences in the authentication mechanism, e.g. using HTTP session cookies rather than BASIC authentication).  However it is likely to be necessary to operate other API endpoints specifically for the use of the web front end - these should be kept to a minimum and where the public endpoints can be used they should be.  It may be necessary for API endpoints to publish permissive CORS policy headers to facilitate this.

## Interaction styles

There are (at least) three distinct styles of interaction which may need to be considered for different tool types and users of the ELG platform:

 - "online" - where the caller sends a single (relatively) small item of data for immediate processing and receives a processed response in near real-time
 - "delayed" - where the caller sends one or more items of data for urgent processing, the API responds immediately with an "accepted" message, runs the process asynchronously and caches the result only until the caller makes another API call to retrieve it.  This approach may be necessary if the processing of a single item is expected to take longer than the typical timeout of a single HTTP request
 - "batch" - where the caller either discovers an ELG public data set in the catalogue or uploads their own data into the ELG for storage, then requests the platform to process the selected data set with one or more tools and save the results in persistent storage for download at a later date

Other possible interaction styles to be considered for later in the project may include push notification of completion via web hooks or streaming processing over WebSockets.

## Existing standard APIs by tool type

### Information Extraction

There are various existing APIs for named entity recognition and similar tasks that could be implemented by the ELG.  Some such as GATE Cloud are fully generic and applicable to any "text in, annotations out" tool, others are more specific, for example enumerating all the allowed annotation types as part of their API specification.

#### GATE Cloud

GATE Cloud provides a very simple API for the "online" style of processing:

 - Input: the text (or data from which text can be extracted, e.g. PDF) directly as the HTTP POST body, with an appropriate `Content-Type` header
 - Output: either JSON in the style of Twitter, or the GATE-specific "GATE XML" standoff XML format
 - Authentication: HTTP BASIC authentication based on randomly generated "API key" username and password pairs.  One user may generate many different API keys and revoke existing keys at any time

Each tool ("pipeline" in GATE Cloud terminology) is published at a unique endpoint address, the desired output format is chosen with the standard HTTP `Accept` header, and all other parameters are optional.  The "Twitter" output format is JSON with two properties, `text` containing the text that was processed (in the case of plain text input this will be identical to the input but in the case of marked up HTML, XML, PDF, etc. it is the plain text extracted from that format), and `entities` representing the annotations:

```json
{
  "text":"Text of the document",
  "entities":{
    "AnnotationType":[
      {
        "indices":[start,end],
        "feature1":"value1",
        // ...
      }
    ]
  }
}
```

Each _type_ of annotation is a key in the entities map, the value of that key is a list of JSON objects, one per annotation, with a property `indices` containing the start and end offsets (in Unicode code points) and arbitrary other properties representing features of the annotation.

GATE Cloud also offers APIs to control batch-mode jobs, where the input and output are abstracted as _data bundles_ of zip files that are ultimately stored in Amazon S3.

#### Amazon Comprehend

Amazon Comprehend is the NLP service provided by the Amazon Web Services public cloud.  It offers various APIs, in both "online" and "batch" styles, for detection of entities, key phrases, tokens and part-of-speech (which they term "syntax").  Amazon Comprehend also provides other similar services for e.g. language detection, which will be described in the relevant sections of this document.

The online mode APIs all take the same input format, which is simple JSON:

```json
{
   "LanguageCode": "string",
   "Text": "string"
}
```

Amazon Comprehend puts the services for all supported languages on the same endpoint, with the language selector as a parameter in the request.  Input is only supported as plain text.

The response format varies between APIs but the different APIs have common elements.  Each API uses a different "wrapper" property at the top level of the response, whose value is an array of objects, one per annotation.  The annotation objects all use `BeginOffset` and `EndOffset` for the annotation location and `Text` for the covered text, plus additional properties for other features.  For example, the named entity service response looks like

```json
{
   "Entities": [
      {
         "BeginOffset": number,
         "EndOffset": number,
         "Score": number,
         "Text": "string",
         "Type": "string"
      }
   ]
}
```

and the "syntax" (tokeniser and POS tagger) looks like

```json
{
   "SyntaxTokens": [
      {
         "BeginOffset": number,
         "EndOffset": number,
         "PartOfSpeech": {
            "Score": number,
            "Tag": "string"
         },
         "Text": "string",
         "TokenId": number
      }
   ]
}
```

For each task at least three different operations are provided, one which takes a single text and returns an immediate response, one which takes an _array_ of (up to 25) texts and returns a matching array of responses or errors, and one which defines an asynchronous batch "job".  The batch APIs are tightly tied to other Amazon Web Services - the input and output are defined in terms of files in Amazon S3, and the credentials to access these locations are provided as an IAM "role".  Operations are provided to start a batch job, monitor its progress, and request it to abort.

Authentication uses the same HMAC signature based mechanism using "access keys" in common with most other Amazon Web Services.

#### Microsoft Azure Cognitive Services

Microsoft Azure provides a simple [entity recognition API](https://westus.dev.cognitive.microsoft.com/docs/services/TextAnalytics-V2-1/operations/5ac4251d5b4ccd1554da7634) operating in the "online" style.  Input is JSON containing one or more plain text documents to process:

```json
{
  "documents": [
    {
      "language": "en",
      "id": "1",
      "text": "Hello world. This is some input text that I love."
    },
    {
      "language": "fr",
      "id": "2",
      "text": "Bonjour tout le monde"
    },
    {
      "language": "es",
      "id": "3",
      "text": "La carretera estaba atascada. Había mucho tráfico el día de ayer."
    }
  ]
}
```

The response is a matching array of results.  The format is entity-oriented rather than mention-oriented, in the sense that there is one entry per _entity_ (linked to Wikipedia) which in turn contains an array of `matches` representing the individual mentions of that entity within the text.

```json
{
  "documents": [
    {
      "id": "1",
      "entities": [
        {
          "name": "Microsoft",
          "matches": [
            {
              "wikipediaScore": null,
              "entityTypeScore": 0.9,
              "text": "MSFT",
              "offset": 0,
              "length": 4
            },
            {
              "wikipediaScore": 0.8,
              "entityTypeScore": 0.9,
              "text": "Microsoft",
              "offset": 25,
              "length": 9
            }
          ],
          "wikipediaLanguage": "en",
          "wikipediaId": "Microsoft",
          "wikipediaUrl": "https://en.wikipedia.org/wiki/Microsoft",
          "bingId": "a093e9b9-90f5-a3d5-c4b8-5855e1b01f85",
          "type": "Organization",
          "subType": null
        }
      ]
    }
  ]
}
```

Authentication is via either a custom HTTP request header containing an API key or a "bearer token" generated by a prior HTTP call (the intention being that the secret API key can be converted to a short-lived bearer token which can more safely be released to non-secure code such as a browser UI).

### Machine Translation

#### Google Cloud Translation API

Google provides a [translation API](https://cloud.google.com/translate/docs/) as part of the Google Cloud Platform.  There is a single endpoint, with the source and target languages specified as parameters in the request.  The current stable version of the API is v2, and the request format is JSON in the POST body, with `Content-Type: application/json`

```json
{
  "q": "The text to translate",
  "source": "en", // optional
  "target": "de",
  "format": "text"
}
```

"format" is either "html" or "text", and the source language is optional - if omitted the service will attempt to determine the source language automatically.  The response is also JSON, a top-level object with a single `data` property, which in turn has a `translations` property which is an array of translations:

```json
{
  "data":{
    "translations":[
      {
        "translatedText":"Der zu übersetzende Text",
        "detectedSourceLanguage":"en" // only if source not specified in request
      }
    ]
  }
}
```

Authentication uses OAuth2 bearer tokens.


### Automatic Speech Recognition

#### Google Cloud Speech-to-Text

Google provides a [speech-to-text](https://cloud.google.com/speech-to-text/docs/) API as part of their Google Cloud platform.  The current REST API supports the "online" and "delayed" interaction styles, with separate endpoints for each, both types take the same request format:

```json
{
  "config":{
    "encoding": enum (AudioEncoding),
    "sampleRateHertz": number,
    "audioChannelCount": number,
    "enableSeparateRecognitionPerChannel": boolean,
    "languageCode": string,
    "maxAlternatives": number,
    "profanityFilter": boolean,
    "speechContexts": [
      {
        object (SpeechContext)
      }
    ],
    "enableWordTimeOffsets": boolean,
    "enableAutomaticPunctuation": boolean,
    "metadata": {
      object (RecognitionMetadata)
    },
    "model": string,
    "useEnhanced": boolean
  },
  "audio":{
    // either/or
    "content":"base64-encoded audio data",
    "uri":"URI to content - Google supports only gs:// URIs to Google cloud storage"
  }
}
```

Supported encodings include lossless `LINEAR16` (16-bit uncompressed) and `FLAC`, plus a number of lossy codecs such as `MULAW` and `OGG_OPUS`.  They provide models for many languages and language variants (e.g. en-GB vs en-US), all accessed via the same endpoint with the language code as a parameter.  The `speechContexts` parameter provides hints to the recogniser, for example if the audio is known to be a controlled vocabulary of commands then the recogniser can be set to weight those command words more heavily when the audio is ambiguous.  The `model` parameter selects from a choice of models tuned to different types of audio (`phone_call`, `video`, `command_and_search` or `default`).

The audio can be provided either as inline base-64 encoded binary data or as a URI reference to a binary blob in Google Cloud Storage.

The "online" mode endpoint processes the audio immediately and returns the transcription in the HTTPS response.  The "delayed" mode endpoint returns immediately with an operation identifier which can be passed to another API endpoint to poll and retrieve the result once the operation completes.  The response (whether immediate or delayed) is in the following format:

```json
{
  "results":[
    {
      "alternatives":[
        {
          "transcript":"Transcript of this section of audio",
          "confidence":number,
          "words":[
            {
              "startTime":"1.0s",
              "endTime":"1.142s",
              "word":"string"
            }
          ]
        }
      ],
      "channelTag": number
    }
  ]
}
```

Individual word start and end times are only returned if requested by `enableWordTimeOffsets` in the request configuration.

In addition to the two REST endpoints, Google also offers a streaming service, but only using the gRPC protocol (messages encoded using protobuf and streamed over HTTP/2) rather than REST.


## Proposal for ELG-specific APIs

Given all locally hosted ELG services will need to conform (directly or via a service adapter) to the message format specifications detailed in [T2.5-api-design.md](T2.5-api-design.md), we propose defining a public REST API based on the same message formats, adapted where necessary.  For example, where there is a need to ship binary data for processing along with the JSON request metadata we propose using the standard MIME multipart mechanism used by web browsers to submit HTML forms containing `<input type="file">`.

Authentication TBC but probably based around OAuth2 bearer tokens

### Information Extraction

For IE services we expect the user to provide data for processing as plain text or some format from which plain text can be extracted (e.g. HTML, PDF) and the response will be the stand-off annotations found in that text.  There are three options for the request format:

**Full JSON request format**

```json
{
  "type":"text",
  "params":{...},   /* optional */
  "content":"The content, as a string inline",
  // mimeType optional - this is the default if omitted
  "mimeType":"text/plain",
  "features":{ /* arbitrary JSON metadata about this content, optional */ },
  "annotations":{ /* optional */
    "<annotation type>":[
      {
        "start":number,
        "end":number,
        "features":{ /* arbitrary JSON */ }
      }
    ]
  }
}
```

`"features"` gives metadata pertaining to the response as a whole, `"annotations"` are metadata pertaining to particular spans of characters within the input, with the start and end representing the position in the `"content"` counted in Unicode code points from the start (counting from zero, start inclusive, end exclusive - alternatively you can think of the numbers as representing the points _between_ characters, with zero before the first character, 1 between the first and second, etc.).

**Request format when no features, annotations or complex parameters are required**

If _no_ `features` or `annotations` are required for a particular request then everything else can be inferred from the `Content-Type` of the request data and the parameters can be constructed from the URL query string.  In this case it is possible to pass just the `content` rather than the full json request:

```
POST ..../processText/service?genre=news&kind=short&kind=breaking
Content-Type: text/plain; charset=UTF-8

Text data goes here
```

This is treated the same as a JSON request of:

```json
{
  "type":"text",
  "content":"Text data goes here",
  "mimeType":"text/plain; charset=UTF-8",
  "params":{
    "genre":"news",
    "kind":["short","breaking"]
  }
}
```

or for an HTML request

```
POST ..../processText/service
Content-Type: text/html; charset=UTF-8

<html>
<body>
<p>Hello world</p>
</body>
</html>
```

is treated as

```json
{
  "type":"text",
  "content":"<html>\n<body>\n....",
  "mimeType":"text/html; charset=UTF-8",
}
```

The response format will be exactly as the "annotations response" message format:

```json
{
  "response":{
    "type":"annotations",
    "warnings":[...], /* optional */
    "features":{...}, /* optional */
    "annotations":{
      "<annotation type>":[
        {
          "start":number,
          "end":number,
          "features":{ /* arbitrary JSON */ }
        }
      ]
    }
  }
}
```

with features and annotations as described above in the request section.

### Text classification

Document classification services that accept textual data can use the same input format options as IE services.  For these services the response will be as per the "classification response" message format:

```json
{
  "response":{
    "type":"classification",
    "warnings":[...], /* optional */
    "classes":[
      {
        "class":"string",
        "score":number /* optional */
      }
    ]
  }
}
```

We allow for zero or more classifications, each with an optional score. Services should return multiple classes in whatever order they feel is most useful (e.g. "most probable class" first), this order need not correspond to a monotonic ordering by score - we don't assume scores are all mutually comparable - and the order will be preserved by subsequent processing steps.

### Machine Translation

For text-to-text MT, the same input formats may be reused as in the IE and text classification cases.  The response this time will be based on the "texts" response message type:

```json
{
  "response":{
    "type":"texts",
    "warnings":[...], /* optional */
    "texts":[
      {
        "role":"string", /* optional */
        "content":"string of translated/transcribed text", // either
        "texts":[/* same structure, recursive */],         // or
        "score":number, /* optional */
        "features":{ /* arbitrary JSON, optional */ },
        "annotations":{ /* optional */
          "<annotation type>":[
            {
              "start":number,
              "end":number,
              "sourceStart":number, // optional
              "sourceEnd":number,   // optional
              "features":{ /* arbitrary JSON */ }
            }
          ]
        }
      }
    ]
  }
}
```

This response structure is recursive, so it is possible for each text in the list to contain a set of other texts instead of a single string.  The texts response type can be used in two different ways, either

- the top-level list of texts is interpreted as a set of _alternatives_ for the whole result - in this case we would expect the `text` property to be populated but not the `texts` one, and a "role" value of "alternative"
  - in this case services should return the alternatives in whatever order they feel is most useful, typically descending order of likelihood (though as for classification results we don't assume scores are mutually comparable and the order of alternatives in the array need not correspond to a monotonic ordering by score).
- the top-level list of texts is interpreted as a set of _segments_ of the result, where each segment can have N-best alternatives (e.g. a list of sentences, with N possible translations for each sentence).  In this case we would expect `texts` to be populated but not `text`, and a "role" value indicating the nature of the segmentation such as "sentence", "paragraph", etc.
  - in this case the order of the texts should correspond to the order of the segments in the result.

This pattern may be continued recursively if appropriate.  Each alternative or segment may optionally contain annotations over sub-ranges of characters within that text or segments within the next level "texts" - the `"start"` and `"end"` markers refer to character offsets within the `"text"` for leaf nodes or array offsets into `"texts"` for branch nodes.  Annotations may optionally be anchored also to locations within the _source_ text via the `"sourceStart"` and `"sourceEnd"` properties.

### Automatic Speech Recognition

ASR services take audio as input and return some sort of text with one or more alternative transcriptions as a result.  The full request format is `multipart/form-data` with JSON request metadata as the first part and the audio data as the second part (in `audio/x-wav` or `audio/mpeg` MP3 format):

```
POST ..../processAudio/service
Content-Type: multipart/form-data; boundary=generated-boundary

--generated-boundary
Content-Disposition: form-data; name="request"
Content-Type: application/json

{
  "type":"audio",
  "params":{...}, // optional
  "format":"string", // LINEAR16 for WAV or MP3 for MP3, other types are service specific
  "sampleRate":number,
  "features":{ /* arbitrary JSON metadata about this content, optional */ },
  "annotations":{ /* optional */
    "<annotation type>":[
      {
        "start":number,
        "end":number,
        "features":{ /* arbitrary JSON */ }
      }
    ]
  }
}
--generated-boundary
Content-Disposition: form-data; name="content"; filename="file.mp3"
Content-Type: audio/mpeg

<MP3 data goes here>
--generated-boundary--
```

The `features` and `annotations` are similar to those described for text requests in the Information Extraction section above, except that the start and end are floating-point numbers representing the time in seconds from the start of the audio.  Alternatively, if no features or annotations are required, and there are either no "params" or just simple params which can be represented as query string parameters (see the IE section above) then the request can be just the audio data alone

```
POST ..../processAudio/service?param1=value1
Content-Type: audio/mpeg

<MP3 data goes here>
```

The response will be as for Machine Translation above, except that the `"sourceStart"` and `"sourceEnd"` in any annotations will refer to timestamps in the audio rather than character offsets.

### Text to Speech

TTS services take text and return an audio rendering of that text as spoken language.  Such services again re-use the text input format from Information Extraction above, but this time return the following JSON response structure:

```json
{
  "response":{
    "type":"audio",
    "warnings":[...], /* optional */
    // either embedded content or contentLocation
    "content":"base64 encoded audio for shorter snippets",
    "format":"string",
    "features":{/* arbitrary JSON, optional */},
    "annotations":{
      "<annotation type>":[
        {
          "start":number,
          "end":number,
          "sourceStart":number, // optional
          "sourceEnd":number,   // optional
          "features":{ /* arbitrary JSON */ }
        }
      ]
    }
  }
}
```

The `annotations` cover sub-ranges of time within the returned audio - the `"start"` and `"end"` markers refer to locations within the `"content"`.  Annotations may optionally be anchored also to locations within the _source_ data (typically character offsets into a source text, for TTS) via the `"sourceStart"` and `"sourceEnd"` properties.

If the service you are calling does not return any features or annotations (or if it does return such but you are not interested in receiving them) you can pass the parameter `audioOnly=true`, in which case you will receive the audio data directly in the HTTP response rather than base64-encoded within the JSON.

```
POST ..../processText/tts-service?audioOnly=true
Content-Type: text/plain

<text to be spoken>
```

Response:

```
Content-Type: audio/mpeg

<MP3 data>
```
